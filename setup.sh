#!/usr/bin/env bash

set -eo pipefail

# Helper variables
tmpPath=.tmp
jwtAccessorFile=${tmpPath}/jwt-accessor

# User controlled variables
VAULT_ADDR=${VAULT_ADDR:-https://vault.maczukin.dev}
GITLAB_HOSTNAME=${GITLAB_HOSTNAME:-gitlab.maczukin.dev}
GITLAB_URL=${GITLAB_URL:-https://${GITLAB_HOSTNAME}}
GITLAB_PROJECT_ID=${GITLAB_PROJECT_ID:-20}

# Vault variables
authPath="${GITLAB_HOSTNAME}/jwt"
pipelineSecretsPath="pipeline-secrets"
kv1SecretsPath="kv1"

roleName="pipeline-job"
policyName="pipeline-job-${GITLAB_HOSTNAME}-${GITLAB_PROJECT_ID}"

_renderIEAMetaKey() {
    local jwtAccessor
    jwtAccessor=$(cat ${jwtAccessorFile})

    echo "{{identity.entity.aliases.${jwtAccessor}.metadata.${1}}}"
}

_renderRole() {
    cat <<EOF
{
    "role_type": "jwt",
    "policies": ["${policyName}"],
    "token_explicit_max_ttl": 60,
    "bound_claims": {
      "project_id": "${GITLAB_PROJECT_ID}"
    },
    "user_claim": "user_email",
    "claim_mappings": {
        "iss": "gitlab_instance",
        "project_id": "project_id",
        "pipeline_id": "pipeline_id"
    }
}
EOF
}

_renderPolicy() {
    cat <<EOF
path "${pipelineSecretsPath}/data/shared" {
    capabilities = ["read", "list"]
}

path "${pipelineSecretsPath}/metadata/instance/$(_renderIEAMetaKey gitlab_instance)/project/$(_renderIEAMetaKey project_id)/pipeline/$(_renderIEAMetaKey pipeline_id)/*" {
    capabilities = ["delete", "list"]
}

path "${pipelineSecretsPath}/data/instance/$(_renderIEAMetaKey gitlab_instance)/project/$(_renderIEAMetaKey project_id)/pipeline/$(_renderIEAMetaKey pipeline_id)/*" {
    capabilities = ["create", "read", "update", "delete", "list"]
}

path "${kv1SecretsPath}/*" {
    capabilities = ["create", "read", "update", "delete", "list"]
}
EOF
}

_msg() {
    echo -e "\033[37;1m"
    echo "${@}"
    echo -e "------------------------------------------------------------\033[0;0m"
}

_enableGitLabJWTAuth() {
    _msg "Enabling JWT auth at ${authPath}"
    vault auth enable -path "${authPath}" jwt

    _msg "Saving ${authPath} accessor at ${jwtAccessorFile}"
    vault auth list -format json | jq -r ".[\"${authPath}/\"].accessor" | tee ${jwtAccessorFile}

    _msg "Defining ${roleName} role"
    _renderRole | vault write "auth/${authPath}/role/${roleName}" -

    _msg "Writing the JWT auth method config"
    vault write "auth/${authPath}/config" \
        jwks_url="${GITLAB_URL}/oauth/discovery/keys" \
        bound_issuer="${GITLAB_HOSTNAME}" \
        default_role="${roleName}"
}

_disableGitLabJWTAuth() {
    _msg "Disabling JWT auth at ${authPath}"
    vault auth disable "${authPath}"
}

_loadPolicies() {
    _msg "Loading ${policyName} policy"
    _renderPolicy | vault policy write "${policyName}" -
}

_clearPolicies() {
    _msg "Removing ${policyName} policy"
    vault policy delete "${policyName}"
}

_enableSecrets() {
    _msg "Enabling KVv2 secrets at ${pipelineSecretsPath}"
    vault secrets enable -path "${pipelineSecretsPath}" kv-v2

    _msg "Enabling KVv1 secrets at ${kv1SecretsPath}"
    vault secrets enable -path "${kv1SecretsPath}" kv-v1
}

_disableSecrets() {
    _msg "Disabling KVv2 secrets at ${pipelineSecretsPath}"
    vault secrets disable "${pipelineSecretsPath}"

    _msg "Disabling KVv1 secrets at ${kv1SecretsPath}"
    vault secrets disable "${kv1SecretsPath}"
}

_writeExampleSharedSecrets() {
    _msg "Writing example secrets at ${pipelineSecretsPath}/shared"
    vault kv put "${pipelineSecretsPath}/shared" KEY_1=VALUE_1 KEY_2=VALUE_2

    _msg "Writing example secrets at ${kv1SecretsPath}/shared"
    vault kv put "${kv1SecretsPath}/test" KEY_1=VALUE_1 KEY_2=VALUE_2
}

_status() {
    _msg "Vault status"
    vault status

    _msg "Vault auth"
    vault auth list

    _msg "Vault secrets"
    vault secrets list

    _msg "Vault policies"
    vault policy list

    _msg "GitLab JWT roles"
    vault list "auth/${authPath}/role"
}

mkdir -p "${tmpPath}"

_cmd_auth_up() {
    _enableGitLabJWTAuth
    _loadPolicies
}

_cmd_auth_down() {
    _clearPolicies
    _disableGitLabJWTAuth
}

_cmd_up() {
    _enableSecrets
    _writeExampleSharedSecrets
}

_cmd_down() {
    _disableSecrets
}

_cmd_status() {
    _status
}

case "${1}" in
    reload-policies)
        _loadPolicies
        ;;
    auth_up)
        _cmd_auth_up
        ;;
    auth_down)
        _cmd_auth_down
        ;;
    up)
        _cmd_up
        ;;
    down)
        _cmd_auth_down
        ;;
    status)
        _cmd_status
        ;;
    force-recreate)
        while read GITLAB_HOSTNAME GITLAB_PROJECT_ID; do
            export GITLAB_HOSTNAME
            export GITLAB_PROJECT_ID
            ${0} auth_down
            ${0} auth_up
        done < installs.txt
        _cmd_down
        _cmd_up
        _cmd_status
        ;;
    *)
        echo "Usage: $0 auth_up|auth_down|up|down|status|force-recreate|reload-policies"
        exit 1
esac

