# Test vault JWT

This project is an example of how GitLab CI's support for HashiCorp Vault's JWT authentication
can be used to pass secrets between jobs in a pipeline.

Original description of the problem can be found at <https://gitlab.com/gitlab-org/gitlab/-/issues/212252>.

## Secrets sharing story

### The problem

Some time ago, during a Slack discussion, I've got a very interesting question:
 
> How we can improve GitLab to pass secrets between jobs in a pipeline without using public
> and unencrypted artifacts for this?

This question addresses a following workflow:

1. In my pipeline I have a stage `deploy` with a job `deploy test`, which deploys my application
   into a test infrastructure that I own.

1. In this pipeline I have another stage `test` with job `test API`, which executes some API tests
   against the deployed application.

1. The API requires authentication and the test credentials are created in the `deploy test` job.
   But they are required in `test API` one.

1. Because of that in the `deploy test` job I put the API token into a file that is next saved as
   an artifact.

1. `test API` job downloads the artifacts, reads the token from the file and successfully executes the test.

The problem with this workflow is that the artifacts are publicly available. So even if this is a test application,
users may leak the credentials to their infrastructure. If the project is public and jobs are public - the credentials
become public as well.

**This is highly unsecure!**

### The solution

At GitLab we've been looking on how to integrate GitLab CI with HashiCorp Vault for a long time now. Which nicely
fits also into this use case

With the _Phase 1_ of [Bring Your Own HashiCorp Vault Integration](https://gitlab.com/groups/gitlab-org/-/epics/2868#phase-1-jwt-authentication-for-bring-your-own-hashicorp-vault)
epic, we will have the powerful mechanism open for us.

GitLab will generate a JSON Web Token, that will be next used to login into a Vault server owned and
configured by the user. With the JWT GitLab will send a set of metadata, that can be used to configure Vault
access policies.

With such Vault configuration prepared in advance, the server being accessible for GitLab Runner that will
handle the job and GitLab being accessible for Vault server, the user will be able to store and fetch
secrets using Vault CLI or interacting directly with it's API.

### Future improvements

Following the _convention over configuration_ approach, we could simplify interaction with Vault for both
secrets defined in advance (group/project Secret Variables; maybe also `.gitlab-ci.yml`'s definition) and for secrets
created in jobs, that should be shared within the pipeline.

The _Phase 2_ of [Bring Your Own HashiCorp Vault Integration](https://gitlab.com/groups/gitlab-org/-/epics/2868#phase-2-fetch-configure-secrets)
epic describes some ideas how we could improve it. But the discussion is still open.

For now, I have some misty idea on how we could combine a new syntax added to YAML for job definition and a small
change in `gitlab-runner-helper` to handle such variables sharing automatically, asking the user only to save
the secret in a predefined format, in a predefined place.

## How to run this example

This example uses the [`CI_JOB_JWT` defined for each job](https://gitlab.com/gitlab-org/gitlab/-/issues/207125) and
`vault` CLI tool to interact with Vault.

The project contains also scripting that will prepare the required configuration on the Vault server.

The project **does not** describe in details how to prepare GitLab and Vault installation and basic setup. This
must be handled by the user. The description will only define what requirements must be met.

### Requirements

1. GitLab instance with the [Generate JWT for authentication](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28063)
   change.

   For my tests of this example, I was using my development environment prepared with [GitLab Compose Kit](https://gitlab.com/gitlab-org/gitlab-compose-kit/).
   I've manually checked out the branch from the Merge Request linked above, and started GitLab from it.

1. An unsealed [HashiCorp Vault server](https://www.vaultproject.io/). For the purpose of this example you can use
   a server started in the development mode and use the initial `root` token. You should be logged into Vault
   when executing the commands from the following steps.

   **However be aware, that such setup is highly discourage for a non-development usage**. You should especially
   not use the server in development mode nor the `root` token for a production usage. In that case a properly
   deployed and secured server, as well as a limited admin account with only required permissions applied should
   be used.

   Make sure that `VAULT_ADDR` variable is set to point your Vault server.

1. [GitLab Runner](https://docs.gitlab.com/runner/) installed and registered within GitLab instance. The Runner should
   be configured to use the [Docker executor](https://docs.gitlab.com/runner/executors/docker.html).
   For the purpose of this example it's not important whether it's a project specific, group or an instance runner.

   What is required, is that the Vault server is accessible from within the containers created by Runner to
   execute the jobs.

1. On my development machine I'm using containerized [Traefik](https://containo.us/traefik/) and a custom, locally
   resolvable domain for all of my applications. Therefore I'm able to use a nice domain names like
   `gitlab.maczukin.dev` or `vault.maczukin.dev` which are reachable also from all my local containers.

   Description of this setup is out of the scope of this example. You however must make sure, that all blocks can
   communicate with each other properly, before you will continue.

### Setup

After all applications are installed and ready, we can proceed with configuring the Vault server for the purpose
of this example.

But first, let's clone the project:

```shell
$ git clone https://gitlab.com/tmaczukin-test-projects/test-vault-jwt.git
$ cd test-vault-jwt
```

Let's also crate a project at our test GitLab instance, where the code will be pushed next. **But please don't
do this push yet!**. For now just create the project and note the ID that was assigned (you can find it at the
project's main page as well as at the main settings page of the project).

Before we continue, you should also make sure that you've configured three variables that control Vault
configuration for JWT authentication method and the applied policies:

```shell
$ export GITLAB_HOSTNAME=gitlab.example.com
$ export GITLAB_URL=http://gitlab.example.com
$ export GITLAB_PROJECT_ID=20
```

Things to remember, are:

- `GITLAB_HOSTNAME` should be equal to the hostname defined in GitLab's `gitlab.yml` file. This value
  will determine variables used by GitLab in the job (e.g. `CI_SERVER_HOST`) and will be set as the
  issuer of the generated JWT.
- `GITLAB_URL` should point an address, under which GitLab will be accessible by the Vault server. The
  setup script will use it to define the URL under which Vault will find the keys to verify JWT
  (`${GITLAB_URL}/oauth/discovery/keys`).
- If your GitLab server is accessible via HTTPS - use it!
- `GITLAB_PROJECT_ID` should of course be equal to the ID of the project that was created a step before.

Having all of the variables defined, let's execute the setup script:

```shell
$ ./setup.sh up
```

In the output we can see that the script:

1. Enables HWT auth method at path defined as `${GITLAB_HOSTNAME}/jwt` and saves the accessor ID:

    ```shell
    Enabling JWT auth at gitlab.maczukin.dev/jwt
    ------------------------------------------------------------
    Success! Enabled jwt auth method at: gitlab.maczukin.dev/jwt/

    Saving gitlab.maczukin.dev/jwt accessor at .tmp/jwt-accessor
    ------------------------------------------------------------
    auth_jwt_d8238001
    ```

1. Defines `pipeline-job` role assigned to the enabled JWT auth method:
   
    ```shell
    Defining pipeline-job role
    ------------------------------------------------------------
    ```

    The role is defined as:

    ```json
    {
        "role_type": "jwt",
        "policies": ["pipeline-job"],
        "token_explicit_max_ttl": 300,
        "bound_subject": "${GITLAB_PROJECT_ID}",
        "user_claim": "user_email",
        "claim_mappings": {
            "iss": "gitlab_instance",
            "project_id": "project_id",
            "pipeline_id": "pipeline_id"
        }
    }
    ```
   
    At this moment the `pipeline-job` policy is not yet defined, but it's not a problem for Vault.
    We will fill this gap in a moment.
    
    The `token_explicit_max_ttl` value set to `300 seconds` forces the maximum TTL of the token generated by
    logging with this JWT role. Even if other default configuration allows a longer TTL.

    The `bound_subject` uses the subject of JWT, which by GitLab is defined as job's project ID. Therefore such
    configuration will be usable only for this one project.

    The `user_claim` value defines which claim from JWT should be used to bind the entity and an alias for the
    identity bound to the generated token. Please note that the setup script doesn't create any identities nor
    aliases upfront, which means that one will be generated by Vault at the login time. In a production scenario
    you would probably prefer to use a normal user accounts available at Vault or to bind this identity with some
    "application" account defined explicitly for the CI job usage of this project. This is crucial for a proper
    auditing.

    `claim_mappings` maps the JWT claims (`iss`, `project_id`, `pipeline_id`) to metadata fields that will be
    assigned to the attached entity alias. The metadata fields will be next used to fill the ACL policy template
    and give access to proper secrets.

1. Writes the configuration of the JWT authentication method that we've just created:
 
    ```shell
    Writing the JWT auth method config
    ------------------------------------------------------------
    Success! Data written to: auth/gitlab.maczukin.dev/jwt/config
    ```

    The configuration is set as:

    ```shell
    $ vault write "auth/${GITLAB_HOSTNAME}/jwt/config" \
            jwks_url="${GITLAB_URL}/oauth/discovery/keys" \
            bound_issuer="${GITLAB_HOSTNAME}" \
            default_role="pipeline-job"
    ```

    where:

    - `jwks_url` points GitLab's URL for OAuth keys discover; Vault will use the keys to validate the JWT,
    - `bound_issure` is set to GitLab's hostname, which must equals to `iss` claim from the JWT; otherwise
       JWT will be not authorized,
    - `default_role` is set to use the `pipeline-job` role that we've just created; with this every JWT login
       that doesn't specify any custom role, will use the `pipeline-job` one by default.

1. Loads `pipeline-job` policy:

    ```shell
    Loading pipeline-job policy
    ------------------------------------------------------------
    Success! Uploaded policy: pipeline-job
    ```

    The policy is defined as:

    ```hcl
    path "pipeline-secrets/data/shared" {
        capabilities = ["read", "list"]
    }
    
    path "pipeline-secrets/metadata/instance/{{identity.entity.aliases.auth_jwt_d8238001.metadata.gitlab_instance}}/project/{{identity.entity.aliases.auth_jwt_d8238001.metadata.project_id}}/pipeline/{{identity.entity.aliases.auth_jwt_d8238001.metadata.pipeline_id}}/*" {
        capabilities = ["delete", "list"]
    }
    
    path "pipeline-secrets/data/instance/{{identity.entity.aliases.auth_jwt_d8238001.metadata.gitlab_instance}}/project/{{identity.entity.aliases.auth_jwt_d8238001.metadata.project_id}}/pipeline/{{identity.entity.aliases.auth_jwt_d8238001.metadata.pipeline_id}}/*" {
        capabilities = ["create", "read", "update", "delete", "list"]
    }
    ```

    Please note, that the template variables are using the `auth_jwt_d8238001` accessor ID of the JWT authorization
    method that was preserved in the firs step.

    The policy gives the logged entity following permissions:

    - read and list secrets at the `pipeline-secrets/shared` path,
    - list and delete metadata of `pipeline-secrets/instance/:gitlab_hostname/project/:project_id/pipeline/:pipeline_id/*` secrets,
    - full CRUD and listing capabilities for the `pipeline-secrets/instance/:gitlab_hostname/project/:project_id/pipeline/:pipeline_id/*` secrets.

    Please note that the secret engine at `pipeline-secrets/` path is not yet configured. Again, it's not a problem
    for Vault at this moment.

1. Enables Key Value version 2 secrets engine at `pipeline-secrets/` path

    ```shell
    Enabling KVv2 secrets at pipeline-secrets
    ------------------------------------------------------------
    Success! Enabled the kv-v2 secrets engine at: pipeline-secrets/
    ```

1. Writes example values to the `pipeline-secrets/shared` secret

    ```shell
    Writing example secrets at pipeline-secrets/shared
    ------------------------------------------------------------
    Key              Value
    ---              -----
    created_time     2020-04-04T02:54:07.708898802Z
    deletion_time    n/a
    destroyed        false
    version          1
    ```

    The written values are:
    
    - `KEY_1` with value `VALUE_1`
    - `KEY_2` with value `VALUE_2`

### Execution

**Notice:** for the documentation purpose I will be using the URL of my own local GitLab instance. For your execution
please adjust the calls properly.

Having the Vault configured and all building blocks ready and connected we can proceed with execution of the
example configuration. For that we will add a new remote to the cloned repository and we will push the code
to our test GitLab instance:

```shell
$ git remote add test ssh://git@gitlab.maczukin.dev:2222/roo/test-vault-jwt.git
$ git push test master
```

And this is all! When the push will be finished, GitLab should trigger a new pipeline.

The pipeline will contain three jobs in three stages:

- `define secrets` job in the `start` stage. This job will login into Vault using JWT generated explicitly for it,
    will try to read the shared secret from `pipeline-secrets/shared` and will try to create a new secret with a random
    key and random value at `pipeline-secrets/instance/:gitlab_hostname/project/:project_id/pipeline/:pipeline_id/test`.
    The same key/value pair will be saved into artifact, so that in a next stage we will be able to  validate if Vault
    stored the proper content.

    <details>
    <summary>Example job output:</summary>

    ```shell
    Running with gitlab-runner 12.10.0~beta.82.g8d21977e (8d21977e)
      on apollo-docker-for-gck wE1mTQPR
    Preparing the "docker" executor
    Using Docker executor with image vault:1.3.4 ...
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Using locally found image version due to if-not-present pull policy
    Using docker image sha256:0c2486a1b22ec0f7b7fbc2c831d172e5687d744a55c2b82bac5ef369e16f8a29 for vault:1.3.4 ...
    Preparing environment
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Running on runner-wE1mTQPR-project-20-concurrent-0 via apollo.h.maczukin.pl...
    Getting source from Git repository
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Fetching changes with git depth set to 50...
    Reinitialized existing Git repository in /builds/root/test-vault-jwt/.git/
    From https://gitlab.maczukin.dev/root/test-vault-jwt
     * [new ref]         refs/pipelines/146 -> refs/pipelines/146
       bb3c530..a0a09e5  master             -> origin/master
    Checking out a0a09e5d as master...
    Removing secrets-to-share.txt

    Skipping Git submodules setup
    Restoring cache
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Downloading artifacts
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Running before_script and script
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    $ vault write -field=token auth/${VAULT_AUTH_PATH}/login jwt="${CI_JOB_JWT}" > ~/.vault-token
    $ vault kv get ${V_SECRETS_PATH}/shared
    ====== Metadata ======
    Key              Value
    ---              -----
    created_time     2020-04-03T23:53:51.005409242Z
    deletion_time    n/a
    destroyed        false
    version          1

    ==== Data ====
    Key      Value
    ---      -----
    KEY_1    VALUE_1
    KEY_2    VALUE_2
    $ randomKey=$(dd if=/dev/urandom count=10 bs=1 | base64 | sha256sum | cut -f 1 -d " ")
    10+0 records in
    10+0 records out
    $ randomValue=$(dd if=/dev/urandom count=10 bs=1 | base64 | sha256sum | cut -f 1 -d " ")
    10+0 records in
    10+0 records out
    $ vault kv put ${V_PIPELINE_PATH}/test ${randomKey:0:10}=${randomValue:0:10}
    Key              Value
    ---              -----
    created_time     2020-04-04T00:15:20.752875675Z
    deletion_time    n/a
    destroyed        false
    version          1
    $ echo "export randomKey=${randomKey:0:10}" >> secrets-to-share.txt
    $ echo "export randomValue=${randomValue:0:10}" >> secrets-to-share.txt
    Running after_script
    Saving cache
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Uploading artifacts for successful job
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Uploading artifacts...
    secrets-to-share.txt: found 1 matching files       
    Uploading artifacts to coordinator... ok            id=1060 responseStatus=201 Created responseStatusCode=201 token=UBQwAmJu
    Job succeeded
    ```

    </details>

- `validate secrets` job in the `end` stage. This job will again login into Vault using a new JWT generated explicitly
    for it, will again try to read the same shared secret and will read the secrets from
    `pipeline-secrets/instance/:gitlab_hostname/project/:project_id/pipeline/:pipeline_id/test`.
    Please note that the Vault command in the job uses the predefined GitLab CI variables to reference the GitLab
    hostname, the project ID and the pipeline ID. And because all jobs are executed on one GitLab instance, in the same
    project and in the same pipeline, the secret path will be exactly the same for both jobs (which is exactly what we
    want!)

    Before the job will read the pipeline specific secret, it will first load the variables passed with the
    artifacts file.
    
    **Warning: Passing secrets through GitLab CI artifacts is a highly risky operation and should be avoided whenever
    possible. This approach here is used to show, that our existing Vault integration is fully able to replace this
    imperfect and risky method! You have been warned - do not do this in your production environments!**

    Reading the secret from Vault is the first part of validation, since the job specifies the key that should be read:

    ```shell
    $ vault kv get -field=${randomKey} ${V_PIPELINE_PATH}/test
    ```

    where `$randomKey` contains the value sourced from the artifact file. If the read will succeed, then the job
    finishes the validation by comparing the value received from Vault with the value sourced from the artifact
    file as `$randomValue`.

    <details>
    <summary>Example job output:</summary>

    ```shell
    Running with gitlab-runner 12.10.0~beta.82.g8d21977e (8d21977e)
      on apollo-docker-for-gck wE1mTQPR
    Preparing the "docker" executor
    Using Docker executor with image vault:1.3.4 ...
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Using locally found image version due to if-not-present pull policy
    Using docker image sha256:0c2486a1b22ec0f7b7fbc2c831d172e5687d744a55c2b82bac5ef369e16f8a29 for vault:1.3.4 ...
    Preparing environment
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Running on runner-wE1mTQPR-project-20-concurrent-0 via apollo.h.maczukin.pl...
    Getting source from Git repository
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Fetching changes with git depth set to 50...
    Reinitialized existing Git repository in /builds/root/test-vault-jwt/.git/
    Checking out a0a09e5d as master...
    Removing secrets-to-share.txt

    Skipping Git submodules setup
    Restoring cache
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Downloading artifacts
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Downloading artifacts for define secrets (1060)...
    Downloading artifacts from coordinator... ok        id=1060 responseStatus=200 OK responseStatusCode=200 token=UBQwAmJu
    Running before_script and script
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    $ vault write -field=token auth/${VAULT_AUTH_PATH}/login jwt="${CI_JOB_JWT}" > ~/.vault-token
    $ vault kv get ${V_SECRETS_PATH}/shared
    ====== Metadata ======
    Key              Value
    ---              -----
    created_time     2020-04-03T23:53:51.005409242Z
    deletion_time    n/a
    destroyed        false
    version          1

    ==== Data ====
    Key      Value
    ---      -----
    KEY_1    VALUE_1
    KEY_2    VALUE_2
    $ source secrets-to-share.txt
    $ echo $randomKey
    651ad8e530
    $ echo $randomValue
    c5b111a871
    $ export receivedValue=$(vault kv get -field=${randomKey} ${V_PIPELINE_PATH}/test)
    $ echo $receivedValue
    c5b111a871
    $ [ "${randomValue}" == "${receivedValue}" ]
    Running after_script
    Saving cache
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Uploading artifacts for successful job
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Job succeeded
    ```

    </details>

- `cleanup` job in the predefined `.post` stage. This job makes sure that the secret at
    `pipeline-secrets/instance/:gitlab_hostname/project/:project_id/pipeline/:pipeline_id/test` is removed with the
    pipeline finalization, so that it doesn't waste space.

    <details>
    <summary>Example job output:</summary>

    ```shell
    Running with gitlab-runner 12.10.0~beta.82.g8d21977e (8d21977e)
      on apollo-docker-for-gck wE1mTQPR
    Preparing the "docker" executor
    Using Docker executor with image vault:1.3.4 ...
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Using locally found image version due to if-not-present pull policy
    Using docker image sha256:0c2486a1b22ec0f7b7fbc2c831d172e5687d744a55c2b82bac5ef369e16f8a29 for vault:1.3.4 ...
    Preparing environment
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Running on runner-wE1mTQPR-project-20-concurrent-0 via apollo.h.maczukin.pl...
    Getting source from Git repository
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Fetching changes with git depth set to 50...
    Reinitialized existing Git repository in /builds/root/test-vault-jwt/.git/
    Checking out a0a09e5d as master...
    Removing secrets-to-share.txt

    Skipping Git submodules setup
    Restoring cache
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Downloading artifacts
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Downloading artifacts for define secrets (1060)...
    Downloading artifacts from coordinator... ok        id=1060 responseStatus=200 OK responseStatusCode=200 token=UBQwAmJu
    Running before_script and script
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    $ vault write -field=token auth/${VAULT_AUTH_PATH}/login jwt="${CI_JOB_JWT}" > ~/.vault-token
    $ vault kv list ${V_PIPELINE_PATH}
    Keys
    ----
    test
    $ vault kv metadata delete ${V_PIPELINE_PATH}/test
    Success! Data deleted (if it existed) at: pipeline-secrets/metadata/instance/gitlab.maczukin.dev/project/20/pipeline/146/test
    $ (vault kv list ${V_PIPELINE_PATH} && (echo "The secrets were not removed"; exit 1)) || echo "Secrets removed"
    No value found at pipeline-secrets/metadata/instance/gitlab.maczukin.dev/project/20/pipeline/146
    Secrets removed
    Running after_script
    Saving cache
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Uploading artifacts for successful job
    Authenticating with credentials from /home/tmaczukin/.docker/config.json
    Job succeeded
    ```

    </details>

If the pipeline succeeded then **congratulations!** You have your first _"secrets passing through GitLab CI pipeline with
HashiCorp vault"_ execution completed!

## Author

2020, Tomasz Maczukin, GitLab Inc.

## License

MIT
